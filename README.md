# kb-git

Git knowledge base

# How to filter git log by excluding specified authors
See discussion at http://dymitruk.com/blog/2012/07/18/filtering-by-author-name/  
Ideal way is using git log built-in functionality.  
See:
```shell
git log --invert-grep --author=name1 --author=name2
```

If you run git command outside repo dir and expect to specify commit range, see:
```shell
git -C ${REPO_DIR} --no-pager \
    log --invert-grep --author=name1 --author=name2 ${OLD_HASH}...${NEW_HASH}
```
